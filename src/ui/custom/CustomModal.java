import javax.swing.*;
import java.awt.*;

public class CustomModal extends JDialog {
    private final GridBagConstraints constraints = new GridBagConstraints();

    public CustomModal(JFrame owner, String title, JLabel message, int width, int height){
        super(owner, title, true);

        setLocation(center(width, height));
        setResizable(false);
        setSize(new Dimension(width, height));
        getContentPane().setBackground(Styles.PRIMARY_BACKGROUND_COLOR);

        setLayout(new GridBagLayout());
        constraints.insets = new Insets(5,30,5,30);
        constraints.weightx = 1.0;
        constraints.gridwidth = 2;
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.anchor = GridBagConstraints.CENTER;
        add(message, constraints);
    }

    public void addButton(JButton button){
        constraints.gridy = 1;
        add(button, constraints);
    }

    public Point center(int width, int height) {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

        int x = (dim.width - width) / 2;
        int y = (dim.height - height) / 2;

        return new Point(x, y);
    }
}
