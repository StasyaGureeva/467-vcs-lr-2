import javax.swing.*;
import java.awt.*;
import java.util.Locale;

public class CustomJButton extends JButton {
    public CustomJButton(String text, Color mainColor, Color hoveredColor) {
        super(text.toUpperCase(Locale.ROOT));
        setBackground(mainColor);
        setBorder(BorderFactory.createLineBorder(Styles.BORDER_COLOR));
        addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                setBackground(hoveredColor);
            }

            @Override
            public void mouseExited(java.awt.event.MouseEvent evt) {
                setBackground(mainColor);
            }
        });
    }
}
